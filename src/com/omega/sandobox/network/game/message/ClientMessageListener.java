package com.omega.sandobox.network.game.message;

import com.jme3.network.AbstractMessage;
import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class ClientMessageListener implements MessageListener<Client> {
    
    private static final Logger logger = Logger.getLogger(ClientMessageListener.class.getName());
    
    @Override
    public void messageReceived(Client source, Message message) {
        if (message instanceof NetworkMessage) {
            logger.log(Level.INFO, "Recv << {0}", message);
            ((NetworkMessage) message).execute(source);
        } else if (message instanceof AbstractMessage) {
            logger.log(Level.INFO, "Recv [Native] << {0}", message);
        } else
            logger.log(Level.WARNING, "Received unhandled message {0}", message.getClass().getSimpleName());
    }
}
