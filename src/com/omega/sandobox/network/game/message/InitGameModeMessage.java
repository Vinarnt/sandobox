package com.omega.sandobox.network.game.message;

import com.jme3.network.Client;
import com.jme3.network.serializing.Serializable;
import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.client.task.loading.GameModeLoadingTask;

/**
 *
 * @author Kyu
 */
@Serializable
public class InitGameModeMessage extends NetworkMessage {

    private int gameMode;
    
    @Override
    public void execute(Client client) {
        GameModeLoadingTask _task = (GameModeLoadingTask) Main.getGameCore().getStateManager().getState(LoadingState.class).getCurrentTask();
        _task.setGameMode(gameMode);
        _task.nextStep();
    }
    
}
