package com.omega.sandobox.network.game.message;

import com.jme3.network.Client;
import com.jme3.network.serializing.Serializable;
import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.client.task.loading.GameMapLoadingTask;

/**
 *
 * @author Kyu
 */
@Serializable
public class InitGameMapMessage extends NetworkMessage {

    private int mapId;
    
    @Override
    public void execute(Client client) {
        GameMapLoadingTask _task = (GameMapLoadingTask) Main.getGameCore().getStateManager().getState(LoadingState.class).getCurrentTask();
        _task.setMapId(mapId);
        _task.nextStep();
    }
    
}
