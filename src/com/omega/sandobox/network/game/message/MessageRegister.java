package com.omega.sandobox.network.game.message;

import com.omega.sandobox.network.game.message.authorization.ServerCapacityMessage;
import com.jme3.network.serializing.Serializer;
import com.omega.sandobox.network.game.message.authorization.ValidatePasswordMessage;
import com.omega.sandobox.network.game.message.authorization.VerifiedPasswordMessage;
import com.omega.sandobox.network.game.message.authorization.WrongPasswordMessage;

/**
 *
 * @author Kyu
 */
public class MessageRegister {

    private static final Class<?>[] toRegister = {
        ValidatePasswordMessage.class,
        WrongPasswordMessage.class,
        VerifiedPasswordMessage.class,
        ServerCapacityMessage.class,
        InitGameMapMessage.class,
        InitGameModeMessage.class};

    public static void register() {
        Serializer.registerClasses(toRegister);
    }
}
