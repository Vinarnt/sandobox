package com.omega.sandobox.network.game.message.authorization;

import com.jme3.network.Client;
import com.jme3.network.serializing.Serializable;
import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.network.game.message.NetworkMessage;
import com.omega.sandobox.client.task.loading.PasswordVerificationTask;

/**
 *
 * @author Kyu
 */
@Serializable
public class VerifiedPasswordMessage extends NetworkMessage {

    public VerifiedPasswordMessage() {
    }

    @Override
    public void execute(Client client) {
        PasswordVerificationTask _task = (PasswordVerificationTask) Main.getGameCore().getStateManager().getState(LoadingState.class).getCurrentTask();
        _task.setIsVerified(true);
        _task.nextStep();
    }
}
