package com.omega.sandobox.network.game.message.authorization;

import com.jme3.network.Client;
import com.jme3.network.serializing.Serializable;
import com.omega.sandobox.network.game.message.NetworkMessage;


/**
 *
 * @author Kyu
 */
@Serializable
public class ValidatePasswordMessage extends NetworkMessage {

    private byte[] password;

    public ValidatePasswordMessage() {
    }
    
    public ValidatePasswordMessage(byte[] password) {
        this.password = password;
    }

    @Override
    public void execute(Client client) {
    }
    
}
