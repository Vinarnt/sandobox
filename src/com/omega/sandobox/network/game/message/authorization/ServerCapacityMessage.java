package com.omega.sandobox.network.game.message.authorization;

import com.jme3.network.Client;
import com.jme3.network.serializing.Serializable;
import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.client.task.loading.ServerCapacityTask;
import com.omega.sandobox.network.game.message.NetworkMessage;

/**
 *
 * @author Kyu
 */
@Serializable
public class ServerCapacityMessage extends NetworkMessage {

    private boolean isFull;

    public ServerCapacityMessage() {
    }

    @Override
    public void execute(Client client) {
        LoadingState _loadState = Main.getGameCore().getStateManager().getState(LoadingState.class);
        ServerCapacityTask _task = (ServerCapacityTask) _loadState.getCurrentTask();
        _task.setIsFull(isFull);
        _task.nextStep();
    }

}
