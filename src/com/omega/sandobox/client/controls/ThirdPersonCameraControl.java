package com.omega.sandobox.client.controls;

import com.jme3.input.ChaseCamera;
import com.jme3.input.InputManager;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Spatial;

public class ThirdPersonCameraControl extends ChaseCamera
{
    
    public ThirdPersonCameraControl(Camera cam, InputManager inputManager)
    {
        // Initialize chaseCamera
        super(cam, inputManager);
        
        this.setLookAtOffset(Vector3f.UNIT_Y.mult(3f));
        this.setInvertVerticalAxis(true);
        this.setDefaultVerticalRotation(0.1f);
        //this.setDefaultHorizontalRotation(FastMath.DEG_TO_RAD * 180.0f);
        this.setDragToRotate(false);
        this.setRotationSpeed(1.0f);
    }
    
    @Override
    public void setSpatial(Spatial spatial)
    {
        super.setSpatial(spatial);
        
        if(spatial == null)
        {
            enabled = false;
            
            return;
        }
        else
            enabled = true;
        
        this.setDragToRotate(dragToRotate);
    }
    
    public void setHorizontalRotation(float rotation)
    {
        this.rotation = rotation;
    }
    
    public void setVerticalRotation(float rotation)
    {
        this.vRotation = rotation;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
