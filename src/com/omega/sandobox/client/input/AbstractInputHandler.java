package com.omega.sandobox.client.input;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.input.InputManager;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;

/**
 *
 * @author Kyu
 */
public abstract class AbstractInputHandler implements ActionListener, AnalogListener {

    protected SimpleApplication app;
    protected InputManager inputManager;

    public AbstractInputHandler(Application app) {
        this.app = (SimpleApplication) app;
        this.inputManager = app.getInputManager();
    }

    public void initialize() {
        registerMappings();
    }
    
    protected abstract void registerMappings();

    public void cleanup() {
        inputManager.removeListener(this);
    }

}
