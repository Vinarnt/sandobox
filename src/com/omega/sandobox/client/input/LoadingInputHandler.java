package com.omega.sandobox.client.input;

import com.jme3.app.Application;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.KeyTrigger;
import com.omega.sandobox.client.appstate.LoadingState;

/**
 *
 * @author Kyu
 */
public class LoadingInputHandler extends AbstractInputHandler {

    private String[] mapping = new String[]{"abortLoading"};
    private LoadingState loadState;
    
    public LoadingInputHandler(Application app) {
        super(app);
    }

    @Override
    public void initialize() {
        loadState = app.getStateManager().getState(LoadingState.class);
        
        super.initialize();
    }
    
    @Override
    protected void registerMappings() {
        inputManager.addMapping(mapping[0], new KeyTrigger(KeyInput.KEY_ESCAPE));
        
        inputManager.addListener(this, mapping);
    }

    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        if(name.equals(mapping[0]) && !isPressed) {
            loadState.abortLoading(0);
        }
    }

    @Override
    public void onAnalog(String name, float value, float tpf) {
    }
    
}
