package com.omega.sandobox.client.input;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.KeyTrigger;
import com.omega.sandobox.client.appstate.GameState;
import java.util.logging.Logger;
import strongdk.jme.appstate.console.ConsoleAppState;

/**
 *
 * @author Kyu
 */
public class CommonGameInputHandler extends AbstractInputHandler {

    private static final Logger logger = Logger.getLogger(CommonGameInputHandler.class.getName());
    private String[] mapping = new String[]{"toogleInterface", "toogleConsole"};
    private ConsoleAppState console;
    private GameState gameState;
    private boolean interfaceMode;

    public CommonGameInputHandler(SimpleApplication app) {
        super(app);
    }

    @Override
    public void initialize() {
        gameState = app.getStateManager().getState(GameState.class);
        console = app.getStateManager().getState(ConsoleAppState.class);
        
        super.initialize();
    }

    @Override
    protected void registerMappings() {
        inputManager.addMapping(mapping[0], new KeyTrigger(KeyInput.KEY_LMENU));
        inputManager.addMapping(mapping[1], new KeyTrigger(KeyInput.KEY_F1));
        
        inputManager.addListener(this, mapping);
    }

    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        if (name.equals(mapping[0])) {
            if (!isPressed) {
                setInterfaceMode(!interfaceMode);
            }
        } else
            if (name.equals(mapping[1])) {
                if (!isPressed) {
                    console.setVisible(!console.isVisible());
                }
            }
    }

    @Override
    public void onAnalog(String name, float value, float tpf) {
    }

    public void setInterfaceMode(boolean isInterfaceMode) {
        this.interfaceMode = isInterfaceMode;

        gameState.getCameraControl().setEnabled(!interfaceMode);
        inputManager.setCursorVisible(interfaceMode);
    }

}
