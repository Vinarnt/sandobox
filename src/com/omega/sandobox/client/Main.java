package com.omega.sandobox.client;

import com.jme3.system.AppSettings;
import com.omega.sandobox.client.appstate.GameCore;

public class Main
{
    
    private static GameCore core;
    
    public static void main(String[] args)
    {
        core = new GameCore();
        AppSettings _settings = new AppSettings(true);
        _settings.setResolution(800, 600);
        core.setSettings(_settings);
        core.setShowSettings(false);
        core.start();            
    }
    
    public static GameCore getGameCore() {
        return core;
    }
}
