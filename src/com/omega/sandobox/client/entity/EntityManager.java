package com.omega.sandobox.client.entity;

import com.jme3.network.Client;
import com.omega.sandobox.client.network.game.ClientFactory;
import com.simsilica.es.client.RemoteEntityData;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class EntityManager {

    private static final Logger logger = Logger.getLogger(EntityManager.class.getName());
    private RemoteEntityData remoteEntityData;
    private Client client;
    private static EntityManager instance;

    public EntityManager() {
    }

    public static EntityManager newInstance() {
        instance = new EntityManager();

        return instance;
    }

    public static EntityManager getInstance() {
        return instance;
    }

    public void initialize() {
        logger.info("Initialize entity system");
        client = ClientFactory.getInstance().getClient();
        remoteEntityData = new RemoteEntityData(client, 0);
    }

    public RemoteEntityData getRemoteEntityData() {
        return remoteEntityData;
    }

    public void close() {
        if(remoteEntityData != null)
            remoteEntityData.close();
    }
}
