package com.omega.sandobox.client.ui.screen;

import com.jme3.app.SimpleApplication;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.client.ui.screen.lobby.PasswordDialog;
import java.util.concurrent.Callable;
import java.util.logging.Logger;
import tonegod.gui.controls.scrolling.ScrollAreaAdapter;
import tonegod.gui.controls.text.Label;
import tonegod.gui.core.Element;
import tonegod.gui.core.Screen;
import tonegod.gui.core.utils.UIDUtil;

/**
 *
 * @author Kyu
 */
public class LoadingScreen extends Element {
    
    private static final Logger logger = Logger.getLogger(LoadingScreen.class.getName());
    private SimpleApplication app;
    private LoadingState loadState;
    private Screen rootScreen;
    private ScrollAreaAdapter labelScroll;
    private short labelsLine;
    private PasswordDialog passwordDialog;
    
    public LoadingScreen(SimpleApplication app, Screen rootScreen) {
         super(
                rootScreen, UIDUtil.getUID(),
                new Vector2f(0f, 0f),
                new Vector2f(rootScreen.getWidth(), rootScreen.getHeight()),
                new Vector4f(0f, 0f, 0f, 0f),
                null
        );
        
        this.app = app;
        this.rootScreen = rootScreen;
        this.loadState = app.getStateManager().getState(LoadingState.class);

        labelScroll = new ScrollAreaAdapter(rootScreen, new Vector2f(10f, 10f), new Vector2f(500f, 300f));
        labelScroll.getVScrollBar().hide();
        addChild(labelScroll);
        
        passwordDialog = new PasswordDialog(rootScreen);
    }
    
    public Label getNextLabel() {
        final Label _nextLabel = new Label(rootScreen, new Vector2f(5f, 5f + (labelsLine * 25f)), new Vector2f(labelScroll.getWidth(), 25f));
        labelsLine++;
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                labelScroll.addScrollableChild(_nextLabel);
                labelScroll.scrollToBottom();
                
                return null;
            }
        });
        
        return _nextLabel;
    }

    public PasswordDialog getPasswordDialog() {
        return passwordDialog;
    } 
}
