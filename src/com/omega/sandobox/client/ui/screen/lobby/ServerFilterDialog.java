package com.omega.sandobox.client.ui.screen.lobby;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.omega.sandobox.client.data.xml.ServerFilterFactory;
import com.omega.sandobox.client.data.ServerFilterData;
import com.omega.sandobox.client.game.mode.common.AbstractGameMode;
import java.util.logging.Logger;
import tonegod.gui.controls.buttons.CheckBox;
import tonegod.gui.controls.lists.SelectBox;
import tonegod.gui.controls.scrolling.ScrollPanel;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.text.TextField;
import tonegod.gui.controls.windows.DialogBox;
import tonegod.gui.core.ElementManager;

/**
 *
 * @author Kyu
 */
public class ServerFilterDialog extends DialogBox {

    private static final Logger logger = Logger.getLogger(ServerFilterDialog.class.getName());
    private ServerFilterData filters;
    private Label nameLabel;
    private TextField nameInput;
    private CheckBox hasPassword;
    private CheckBox isDedicated;
    private CheckBox isFull;
    private CheckBox isEmpty;
    private Label gameModeLabel;
    private SelectBox gameMode;
    private SelectBox map;
    private float leftPadding = 10f;
    private float vSpacing = 10f;

    public ServerFilterDialog(ElementManager screen, ServerFilterData filters, boolean show) {
        super(screen, new Vector2f((screen.getWidth() / 2) - 150f, (screen.getHeight() / 2) - 200f), new Vector2f(400f, 400));

        this.filters = filters;

        setWindowTitle("Server filters");
        setIsMovable(false);
        setIsResizable(false);
        getDragBar().setIsMovable(false);

        setButtonCancelText("Cancel");
        setButtonOkText("Apply");

        ScrollPanel _body = getMessageArea();

        nameLabel = new Label(screen, new Vector2f(leftPadding, 10f), new Vector2f(100f, 15f));
        nameLabel.setText("Server name : ");
        _body.addChild(nameLabel);

        nameInput = new TextField(screen, new Vector2f(leftPadding + 100f, 10f), new Vector2f(200f, 25f));
        nameInput.setText(filters.getName());
        _body.addChild(nameInput);

        hasPassword = new CheckBox(screen, new Vector2f(leftPadding, 25f + vSpacing));
        hasPassword.setLabelText("Show private servers");
        hasPassword.setIsCheckedNoCallback(filters.hasPassword());
        _body.addChild(hasPassword);

        isDedicated = new CheckBox(screen, new Vector2f(leftPadding, 50f + vSpacing));
        isDedicated.setLabelText("Show only dedicated servers");
        isDedicated.setIsCheckedNoCallback(filters.isDedicated());
        _body.addChild(isDedicated);

        isFull = new CheckBox(screen, new Vector2f(leftPadding, 75f + vSpacing));
        isFull.setLabelText("Show full servers");
        isFull.setIsCheckedNoCallback(filters.isFull());
        _body.addChild(isFull);

        isEmpty = new CheckBox(screen, new Vector2f(leftPadding, 100f + vSpacing));
        isEmpty.setLabelText("Show empty servers");
        isEmpty.setIsCheckedNoCallback(filters.isEmpty());
        _body.addChild(isEmpty);

        gameModeLabel = new Label(screen, new Vector2f(leftPadding, 125f + vSpacing), new Vector2f(100f, 25f));
        gameModeLabel.setText("Game mode : ");
        _body.addChild(gameModeLabel);

        gameMode = new SelectBox(screen, new Vector2f(100f + leftPadding, 125f + vSpacing), new Vector2f(200f, 25f)) {
            @Override
            public void onChange(int selectedIndex, Object value) {
                setSelectedIndex(selectedIndex);
            }
        };
        for (AbstractGameMode _mode : AbstractGameMode.getGameModes())
            gameMode.addListItem(_mode.getName(), _mode);

        
        gameMode.setSelectedByCaption(filters.getGameMode(), false);
        _body.addChild(gameMode);

        if(!show)
            hide();
    }

    public void reset() {
        nameInput.setText(filters.getName());
        hasPassword.setIsCheckedNoCallback(filters.hasPassword());
        isDedicated.setIsCheckedNoCallback(filters.isDedicated());
        isFull.setIsCheckedNoCallback(filters.isFull());
        isEmpty.setIsCheckedNoCallback(filters.isEmpty());
        gameMode.setSelectedByCaption(filters.getGameMode(), false);
    }

    @Override
    public void onButtonCancelPressed(MouseButtonEvent evt, boolean toggled) {
        hide();
        reset();
    }

    @Override
    public void onButtonOkPressed(MouseButtonEvent evt, boolean toggled) {
        filters.setName(nameInput.getText());
        filters.setHasPassword(hasPassword.getIsChecked());
        filters.setIsDedicated(isDedicated.getIsChecked());
        filters.setIsFull(isFull.getIsChecked());
        filters.setIsEmpty(isEmpty.getIsChecked());
        filters.setGameMode(((AbstractGameMode) gameMode.getSelectedListItem().getValue()).getName());

        ServerFilterFactory.getInstance().save();
        hide();
    }
}
