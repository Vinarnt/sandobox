package com.omega.sandobox.client.ui.screen.lobby;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.client.appstate.LobbyState;
import com.omega.sandobox.client.appstate.MainMenuState;
import com.omega.sandobox.client.data.xml.ServerFilterFactory;
import com.omega.sandobox.client.data.ServerData;
import com.omega.sandobox.client.network.master.MasterClientFactory;
import com.omega.sandobox.client.ui.ServerEntryRow;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import tonegod.gui.controls.buttons.Button;
import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.lists.Table;
import tonegod.gui.core.Element;
import tonegod.gui.core.Screen;
import tonegod.gui.core.utils.UIDUtil;

/**
 *
 * @author Kyu
 */
public class LobbyScreen extends Element {

    private static final Logger logger = Logger.getLogger(LobbyScreen.class.getName());
    private SimpleApplication app;
    private Screen rootScreen;
    private AppStateManager stateManager;
    private Table serverTable;
    private ServerFilterDialog dialogFilter;
    private Button cancelButton;
    private Button refreshButton;
    private Button filterButton;
    private Button connectButton;
    private ServerInfoFragment infoPanel;
    private long lastCLick;
    private final int doubleClickThreshold = 500; // in ms
    private final float NANO_TO_MS = 0.000001f;

    public LobbyScreen(SimpleApplication app, Screen rootScreen) {
        super(
                rootScreen, UIDUtil.getUID(), 
                new Vector2f(0f, 0f), 
                new Vector2f(rootScreen.getWidth(), rootScreen.getHeight()),
                new Vector4f(0f, 0f, 0f, 0f),
                null
        );
        
        this.app = app;
        this.rootScreen = rootScreen;
        this.stateManager = app.getStateManager();

        app.getInputManager().setCursorVisible(true);

        ServerFilterFactory.getInstance().initialize();
        ServerFilterFactory.getInstance().load();

        serverTable = new Table(rootScreen, new Vector2f(10f, 10f),
                new Vector2f(
                rootScreen.getWidth() - 20f,
                rootScreen.getHeight() - 200f)) {
            @Override
            public void onChange() {
                infoPanel.populate(((ServerEntryRow) serverTable.getSelectedRows().get(0)).getServerData());
            }

            @Override
            public void onKeyRelease(KeyInputEvent evt) {
                if (evt.getKeyCode() == KeyInput.KEY_RETURN)
                    connectTo();
            }

            @Override
            public void onMouseLeftPressed(MouseButtonEvent evt) {
                long _currentTime = evt.getTime();
                long _ellapsedTime = (long) ((_currentTime - lastCLick) * NANO_TO_MS);
                if (_ellapsedTime < doubleClickThreshold)
                    connectTo();

                lastCLick = _currentTime;
            }
        };
        serverTable.setSelectionMode(Table.SelectionMode.ROW);
        serverTable.setSortable(true);
        serverTable.addColumn("Name");
        serverTable.addColumn("Game Mode");
        serverTable.addColumn("Map");
        serverTable.addColumn("Players");
        serverTable.setColumnResizeMode(Table.ColumnResizeMode.AUTO_ALL);
        addChild(serverTable);

        infoPanel = new ServerInfoFragment(rootScreen, new Vector2f(10f, serverTable.getHeight() + 10f), new Vector2f(serverTable.getWidth(), 100f));
        addChild(infoPanel);

        cancelButton = new ButtonAdapter(rootScreen, new Vector2f(
                100f,
                serverTable.getHeight() + infoPanel.getHeight() + 25f),
                new Vector2f(200f, 25f)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
                stateManager.getState(LobbyState.class).setEnabled(false);
                stateManager.getState(MainMenuState.class).setEnabled(true);
            }
        };
        cancelButton.setText("Back");

        addChild(cancelButton);
        refreshButton = new ButtonAdapter(
                rootScreen,
                new Vector2f(
                (rootScreen.getWidth() / 2) - 100f,
                serverTable.getHeight() + infoPanel.getHeight() + 25f), new Vector2f(200f, 25f)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
                logger.info("Refresh server list");
                serverTable.removeAllRows();
                infoPanel.reset();
                MasterClientFactory.getInstance().connect();
            }
        };
        refreshButton.setText("Refresh server list");
        addChild(refreshButton);
        
        dialogFilter = new ServerFilterDialog(rootScreen, ServerFilterFactory.getInstance().getFilters(), false);

        filterButton = new ButtonAdapter(rootScreen, new Vector2f(
                refreshButton.getX() + refreshButton.getWidth(),
                serverTable.getHeight() + infoPanel.getHeight() + 25f),
                new Vector2f(200f, 25f)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
                dialogFilter.showAsModal(true);
            }
        };
        filterButton.setText("Filters ...");
        addChild(filterButton);

        connectButton = new ButtonAdapter(rootScreen, new Vector2f(
                refreshButton.getX(),
                serverTable.getHeight() + infoPanel.getHeight() + refreshButton.getHeight() + 35f),
                new Vector2f(200f, 25f)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
                connectTo();
            }
        };
        connectButton.setText("Connect");
        addChild(connectButton);

        // For dev purpose
        addServerRow(new ServerData("Server Test", "127.0.0.1", 26001, true, false, 0, 16, "Team Death Match", "Test Map"));
    }

    private void connectTo() {
        if (serverTable.getSelectedRows().isEmpty())
            return;

        final ServerData _server = ((ServerEntryRow) serverTable.getSelectedRows().get(0)).getServerData();
        logger.log(Level.INFO, "Connect to {0}", _server.getName());
        LoadingState _loadingState = new LoadingState(_server, rootScreen);
        stateManager.attach(_loadingState);
        stateManager.getState(LobbyState.class).setEnabled(false);
    }

    public void addServerRow(final ServerData data) {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                ServerEntryRow _row = new ServerEntryRow(data, LobbyScreen.this.rootScreen, serverTable);
                serverTable.addRow(_row);

                return null;
            }
        });

    }
}
