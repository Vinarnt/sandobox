package com.omega.sandobox.client.ui.screen.lobby;

import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont.Align;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.client.appstate.LobbyState;
import com.omega.sandobox.client.task.loading.PasswordVerificationTask;
import java.util.concurrent.Callable;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.text.Password;
import tonegod.gui.controls.windows.DialogBox;
import tonegod.gui.core.ElementManager;

/**
 *
 * @author Kyu
 */
public class PasswordDialog extends DialogBox {

    private Label messageLabel;
    private Password passwordInput;
    private String salt;

    public PasswordDialog(ElementManager screen) {
        super(screen, new Vector2f((screen.getWidth() / 2) - 150f, (screen.getHeight() / 2) - 100f), new Vector2f(300f, 200f));

        setIsResizable(false);
        setWindowIsMovable(false);
        setWindowTitle("Password needed");

        messageLabel = new Label(screen, new Vector2f(0f, 5f), new Vector2f(getMessageArea().getWidth(), 25f));
        messageLabel.setText("Enter password");
        messageLabel.setTextAlign(Align.Center);
        getMessageArea().addChild(messageLabel);

        passwordInput = new Password(screen, new Vector2f((getMessageArea().getWidth() / 2) - 50f, 35f), new Vector2f(100f, 25f));
        getMessageArea().addChild(passwordInput);

        hide();
    }

    @Override
    public void onButtonCancelPressed(MouseButtonEvent evt, boolean toggled) {
        AppStateManager _stateManager = Main.getGameCore().getStateManager();
        _stateManager.detach(_stateManager.getState(LoadingState.class));
        _stateManager.getState(LobbyState.class).setEnabled(true);
    }

    @Override
    public void onButtonOkPressed(MouseButtonEvent evt, boolean toggled) {
        final String _password = passwordInput.getText();
        
        hideDialog();
        PasswordVerificationTask _task = (PasswordVerificationTask) app.getStateManager().getState(LoadingState.class).getCurrentTask();
        _task.setPassword(_password);
        _task.setSalt(salt);
        _task.nextStep();
    }

    public void reset() {
        passwordInput.setText("");
    }

    public void showDialog() {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                showAsModal(true);

                return null;
            }
        });
    }

    public void hideDialog() {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                hide();
                reset();

                return null;
            }
        });
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

}
