package com.omega.sandobox.client.ui.screen.lobby;

import com.jme3.math.Vector2f;
import com.omega.sandobox.client.data.ServerData;
import tonegod.gui.controls.extras.SpriteElement;
import tonegod.gui.controls.text.Label;
import tonegod.gui.core.Screen;

/**
 *
 * @author Kyu
 */
public class ServerInfoFragment extends SpriteElement {

    private SpriteElement thumb;
    private String serverNameString = "Server name : ";
    private Label serverName;
    private String playersString = "Players : ";
    private Label players;
    private String gameModeString = "Game Mode : ";
    private Label gameMode;
    private String mapString = "Map : ";
    private Label map;

    public ServerInfoFragment(Screen screen, Vector2f position, Vector2f dimensions) {
        super(screen, position, dimensions);
        initialize(screen);
    }

    private void initialize(Screen screen) {
        thumb = new SpriteElement(screen, new Vector2f(5f, 5f), new Vector2f(getHeight() - 10f, getHeight() - 10f));
        addChild(thumb);

        serverName = new Label(screen, new Vector2f(thumb.getAbsoluteWidth(), 5f), new Vector2f(getWidth(), 25f));
        addChild(serverName);

        players = new Label(screen, new Vector2f(thumb.getAbsoluteWidth(), 25f), new Vector2f(200f, 25f));
        addChild(players);

        gameMode = new Label(screen, new Vector2f(thumb.getAbsoluteWidth(), 45f), new Vector2f(200f, 25f));
        addChild(gameMode);

        map = new Label(screen, new Vector2f(thumb.getAbsoluteWidth(), 65f), new Vector2f(200f, 25f));
        addChild(map);
        
        reset();
    }

    public void populate(ServerData data) {
        serverName.setText(serverNameString + data.getName());
        players.setText(playersString + data.getCurrentPlayers() + "/" + data.getMaxPlayers());
        gameMode.setText(gameModeString + data.getGameMode());
        map.setText(mapString + data.getMap());
    }

    public void reset() {
        serverName.setText(serverNameString);
        players.setText(playersString);
        gameMode.setText(gameModeString);
        map.setText(mapString);
    }
}
