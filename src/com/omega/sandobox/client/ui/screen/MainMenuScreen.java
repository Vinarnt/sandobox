package com.omega.sandobox.client.ui.screen;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;
import com.omega.sandobox.client.appstate.LobbyState;
import com.omega.sandobox.client.appstate.MainMenuState;
import tonegod.gui.controls.buttons.Button;
import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.extras.SpriteElement;
import tonegod.gui.core.Element;
import tonegod.gui.core.Screen;
import tonegod.gui.core.utils.UIDUtil;
import tonegod.gui.effects.Effect;

/**
 *
 * @author Kyu
 */
public class MainMenuScreen extends Element {

    private AppStateManager stateManager;
    private Effect fadeIn;
    private Effect fadeOut;
    private SpriteElement contentPanel;
    private Button joinServerButton;
    private Button createServerButton;
    private Button settingsButton;
    private Button exitButton;
    private float vSpacing = 10f;

    public MainMenuScreen(SimpleApplication app, Screen rootScreen) {
        super(
                rootScreen, UIDUtil.getUID(), 
                new Vector2f(0f, 0f), 
                new Vector2f(rootScreen.getWidth(), rootScreen.getHeight()),
                new Vector4f(0f, 0f, 0f, 0f),
                null
        );
        
        stateManager = app.getStateManager();

        app.getInputManager().setCursorVisible(true);
        contentPanel = new SpriteElement(rootScreen,
                new Vector2f((rootScreen.getWidth() / 2) - 150f, (rootScreen.getHeight() / 2) - 150f),
                new Vector2f(300, 300));
        contentPanel.setIsMovable(false);
        contentPanel.setIsResizable(false);

        joinServerButton = new ButtonAdapter(rootScreen, new Vector2f(50f, 10f + vSpacing), new Vector2f(200f, 25f)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
                stateManager.getState(MainMenuState.class).setEnabled(false);
                stateManager.getState(LobbyState.class).setEnabled(true);
            }
        };
        joinServerButton.setText("Join server");
        contentPanel.addChild(joinServerButton);

        createServerButton = new ButtonAdapter(rootScreen, new Vector2f(50f, 45f + vSpacing), new Vector2f(200f, 25f)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
            }
        };
        createServerButton.setText("Create server");
        contentPanel.addChild(createServerButton);

        settingsButton = new ButtonAdapter(rootScreen, new Vector2f(50f, 80f + vSpacing), new Vector2f(200f, 25f)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
            }
        };
        settingsButton.setText("Settings");
        contentPanel.addChild(settingsButton);

        exitButton = new ButtonAdapter(rootScreen, new Vector2f(50f, 115f + vSpacing), new Vector2f(200f, 25f)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
                app.stop();
            }
        };
        exitButton.setText("Exit");
        contentPanel.addChild(exitButton);

        addChild(contentPanel);
    }
}
