package com.omega.sandobox.client.ui;

import com.omega.sandobox.client.data.ServerData;
import tonegod.gui.controls.lists.Table;
import tonegod.gui.core.ElementManager;

/**
 *
 * @author Kyu
 */
public class ServerEntryRow extends Table.TableRow {

    private ServerData data;
    
    public ServerEntryRow(ServerData data, ElementManager screen, Table table) {
        super(screen, table);
        
        addCell(data.getName(), data.getName());
        addCell(data.getGameMode(), data.getGameMode());
        addCell(data.getMap(), data.getMap());
        addCell(String.valueOf(data.getCurrentPlayers()) + "/" + data.getMaxPlayers(), data.getCurrentPlayers());
        
        this.data = data;
    }
    
    public ServerData getServerData()
    {
        return data;
    }
    
}
