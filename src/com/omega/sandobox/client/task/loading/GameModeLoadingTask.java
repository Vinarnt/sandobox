package com.omega.sandobox.client.task.loading;

import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.game.mode.common.AbstractGameMode;

/**
 *
 * @author Kyu
 */
public class GameModeLoadingTask extends AbstractLoadingTask {

    private int gameMode;

    public GameModeLoadingTask() {
        super("Loading Game Mode : ");
    }

    @Override
    public Void call() throws Exception {

        loadStateLabel = loadScreen.getNextLabel();
        setLabelPrefix("Initialize ...");
        AbstractGameMode _gameMode = AbstractGameMode.getGameModeById(gameMode);
        if (_gameMode != null) {
            Main.getGameCore().getStateManager().attach(_gameMode);
            setLabelPrefix("Ok");
        } else {
            onFailed("Unable to get game mode !", null);
        }

        return null;
    }

    public void setGameMode(int gameMode) {
        this.gameMode = gameMode;
    }

}
