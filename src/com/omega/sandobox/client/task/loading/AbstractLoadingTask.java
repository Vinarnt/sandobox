package com.omega.sandobox.client.task.loading;

import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.client.ui.screen.LoadingScreen;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import tonegod.gui.controls.text.Label;

/**
 *
 * @author Kyu
 */
public abstract class AbstractLoadingTask implements Callable<Void> {

    private static final Logger logger = Logger.getLogger(AbstractLoadingTask.class.getName());
    protected Label loadStateLabel;
    private String labelSuffix;
    protected LoadingState loadState;
    protected LoadingScreen loadScreen;
    private boolean isCancelling;
    private Future future;

    public AbstractLoadingTask(String labelSuffix) {
        this.labelSuffix = labelSuffix;
        this.loadState = Main.getGameCore().getStateManager().getState(LoadingState.class);
        this.loadScreen = loadState.getLoadingScreen();
    }

    protected void setLabelPrefix(final String prefix) {
        Main.getGameCore().enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                loadStateLabel.setText(labelSuffix + prefix);

                return null;
            }
        });
    }

    protected void onFailed(String msg, Throwable t) {
        setLabelPrefix(msg);
        cancel();
        loadState.abortLoading(3);
        logger.log(Level.SEVERE, msg, t);
    }

    public void cancel() {
        if (future == null)
            return;

        isCancelling = true;
        nextStep();
    }

    public void waitNextStep() {
        synchronized (this) {
            try {
                wait();
            } catch (InterruptedException ex) {
                logger.log(Level.SEVERE, "Interrupted wait", ex);
            }
        }
    }

    public void nextStep() {
        synchronized (this) {
            notify();
            if (isCancelling) {
                future.cancel(true);
            }
        }
    }

    public void setFuture(Future future) {
        this.future = future;
    }

}
