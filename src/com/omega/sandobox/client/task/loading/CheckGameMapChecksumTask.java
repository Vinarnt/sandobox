package com.omega.sandobox.client.task.loading;

import com.jme3.asset.AssetInfo;
import com.jme3.asset.AssetKey;
import com.jme3.asset.AssetManager;
import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.game.level.GameMapLoaderHandler;
import com.omega.sandobox.client.network.game.crypto.CryptoManager;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Kyu
 */
public class CheckGameMapChecksumTask extends AbstractLoadingTask {

    private AssetManager assetMgr;
    private AssetKey levelKey;

    public CheckGameMapChecksumTask() {
        super("Verifying level checksum : ");

        assetMgr = Main.getGameCore().getAssetManager();
    }

    @Override
    public Void call() throws Exception {
        try {
            levelKey = GameMapLoaderHandler.getLastLoaded().getLoaderHandler().getLevelKey();
            loadStateLabel = loadScreen.getNextLabel();
            setLabelPrefix("...");

            AssetInfo _levelInfo = assetMgr.locateAsset(levelKey);
            byte[] _checksum = CryptoManager.getInstance().getGmChecksumHandler().generateChecksum(_levelInfo);
            setLabelPrefix("Ok");
        } catch (IOException ex) {
            onFailed("Unable to generate checksum !", ex);
        } catch (NoSuchAlgorithmException ex) {
            onFailed("Checksum algorithm not found !", ex);
        }

        return null;
    }

}
