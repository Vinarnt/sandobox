package com.omega.sandobox.client.task.loading;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.omega.sandobox.client.appstate.GameState;
import com.omega.sandobox.client.appstate.LoadingState;

/**
 *
 * @author Kyu
 */
public class SwitchToGameTask extends AbstractLoadingTask {

    private Application app;
    
    public SwitchToGameTask(Application app) {
        super("Going to game");
        
        this.app = app;
    }
    
    @Override
    public Void call() throws Exception {
        AppStateManager _stateMgr = this.app.getStateManager();
        _stateMgr.attach(new GameState());
        _stateMgr.detach(_stateMgr.getState(LoadingState.class));
        
        return null;
    }
}
