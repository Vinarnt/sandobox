package com.omega.sandobox.client.task.loading;

import com.omega.sandobox.client.data.ServerData;
import com.omega.sandobox.client.network.game.ClientFactory;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class ConnectToServerTask extends AbstractLoadingTask {

    private ServerData data;

    public ConnectToServerTask(ServerData data) {
        super("Connection to server : ");
        this.data = data;
    }

    @Override
    public Void call() throws Exception {
        loadStateLabel = loadScreen.getNextLabel();
        setLabelPrefix("...");
        
        final ClientFactory _clientFact = ClientFactory.getInstance();
        _clientFact.setOnConnectedTask(new Callable<Void>() {
            @Override
            public Void call() {
                setLabelPrefix("Ok");
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ConnectToServerTask.class.getName()).log(Level.SEVERE, null, ex);
                }
                nextStep();

                return null;
            }
        });
        _clientFact.setOnFailedTask(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                onFailed("Unable to connect to server", null);

                return null;
            }
        });
        _clientFact.connect(data);
        waitNextStep();

        return null;
    }

}
