package com.omega.sandobox.client.task.loading;

import com.omega.sandobox.client.game.level.GameMap;
import com.omega.sandobox.client.network.game.ClientFactory;
import com.omega.sandobox.network.game.message.InitGameMapMessage;

/**
 *
 * @author Kyu
 */
public class GameMapLoadingTask extends AbstractLoadingTask {

    private int mapId;
    private ClientFactory clientFact;

    public GameMapLoadingTask() {
        super("Loading level : ");
        
        clientFact = ClientFactory.getInstance();
    }

    @Override
    public Void call() throws Exception {
        loadStateLabel = loadScreen.getNextLabel();
        GameMap _level = null;

        setLabelPrefix("...");
        clientFact.send(new InitGameMapMessage());
        
        waitNextStep();
        for (GameMap _clevel : GameMap.getMapRef()) {
            if (_clevel.getMapId() == mapId) {
                _level = _clevel;
            }
        }

        if (_level == null) {
            onFailed("Map not found !", null);
        } else {
            boolean _isLoaded = _level.getLoaderHandler().load();
            if(_isLoaded)
                setLabelPrefix("Ok");
            else
                onFailed("Unable to load map !", null);
        }

        return null;
    }
    
    public void setMapId(int id) {
        this.mapId = id;
    }
    
}
