package com.omega.sandobox.client.task.loading;

import com.omega.sandobox.client.network.game.ClientFactory;
import com.omega.sandobox.network.game.message.authorization.ServerCapacityMessage;

/**
 *
 * @author Kyu
 */
public class ServerCapacityTask extends AbstractLoadingTask {

    private ClientFactory clientFact;
    private boolean isFull;

    public ServerCapacityTask() {
        super("Check server capacity : ");
        clientFact = ClientFactory.getInstance();
    }

    @Override
    public Void call() throws Exception {
        loadStateLabel = loadScreen.getNextLabel();
        setLabelPrefix("...");
        clientFact.send(new ServerCapacityMessage());

        waitNextStep();
        if (isFull) {
            onFailed("Server is full !", null);
        } else {
            setLabelPrefix("Ok");
        }

        return null;
    }

    public void setIsFull(boolean isFull) {
        this.isFull = isFull;
    }

}
