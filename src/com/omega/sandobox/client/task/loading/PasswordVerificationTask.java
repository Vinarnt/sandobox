package com.omega.sandobox.client.task.loading;

import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.network.game.ClientFactory;
import com.omega.sandobox.client.network.game.crypto.CryptoManager;
import com.omega.sandobox.network.game.message.authorization.ValidatePasswordMessage;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.concurrent.Callable;

/**
 *
 * @author Kyu
 */
public class PasswordVerificationTask extends AbstractLoadingTask {

    private String password;
    private String salt;
    private boolean isVerified;

    public PasswordVerificationTask() {
        super("Checking password : ");
    }

    @Override
    public Void call() throws Exception {
        if (!ClientFactory.getInstance().getTargetServer().hasPassword())
            return null;

        loadStateLabel = loadScreen.getNextLabel();
        setLabelPrefix("...");
        Main.getGameCore().enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                loadState.getLoadingScreen().getPasswordDialog().setSalt(
                        ClientFactory.getInstance().getTargetServer().getName());
                loadState.getLoadingScreen().getPasswordDialog().showDialog();

                return null;
            }
        });

        waitNextStep();
        byte[] _encryptedPass = null;
        try {
        _encryptedPass = CryptoManager.getInstance()
                .getJoinServerPasswordHandlerHandler().encrypt(password, salt);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            onFailed("Unable to encrypt password !", ex);
        }
        ClientFactory.getInstance().send(new ValidatePasswordMessage(_encryptedPass));

        waitNextStep();
        if (isVerified) {
            setLabelPrefix("Ok");
        } else {
            onFailed("Wrong password !", null);
        }

        return null;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public void setIsVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

}
