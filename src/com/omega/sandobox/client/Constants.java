package com.omega.sandobox.client;

/**
 *
 * @author Kyu
 */
public class Constants {
    public static final String MASTER_SERVER_HOST = "127.0.0.1";
    public static final int MASTER_SERVER_PORT = 26002;
    public static final String CLIENT_SERVER_NAME = "sandobox";
    public static final int CLIENT_SERVER_VERSION = 1;
    public static final String SERVER_FILTER_PATH = "config/server_filters.xml";
    public static final float UI_BASE_WIDTH = 800f;
    public static final float UI_BASE_HEIGHT = 600f;
    public static final String ASSETS_FOLDER = "data";
}
