package com.omega.sandobox.client.game.level;

import com.jme3.scene.Spatial;
import com.omega.sandobox.client.Constants;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class GameMap {

    private static final Logger logger = Logger.getLogger(GameMap.class.getName());
    private static final List<GameMap> mapRef = new ArrayList<>();
    private int mapId;
    private String mapName;
    private Spatial level;
    private GameMapLoaderHandler loaderHandler;

    private GameMap() {
        loaderHandler = new GameMapLoaderHandler(this);
    }

    public GameMap(int mapId) {
        this.mapId = mapId;
    }

    public static void referenceMaps() {
        logger.info("Reference levels");
        File _rootDir = new File(Constants.ASSETS_FOLDER + File.separatorChar + "levels");
        if (_rootDir.exists() && _rootDir.isDirectory()) {
            File[] _levelsDir = _rootDir.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.isDirectory();
                }
            });
            for (File _levelDir : _levelsDir) {
                if (_levelDir.isDirectory()) {
                    GameMap _level = new GameMap();
                    logger.log(Level.INFO, "Level : {0}", _levelDir);
                    File[] _levelsFile = _levelDir.listFiles(new FilenameFilter() {
                        @Override
                        public boolean accept(File dir, String name) {
                            return name.endsWith(".j3o") || name.endsWith(".j3odata");
                        }
                    });
                    for (File _levelFile : _levelsFile) {
                        logger.log(Level.INFO, "Content : {0}", _levelFile.getName());
                        if (_levelFile.getName().endsWith(".j3o")) {
                            _level.getLoaderHandler().setMapDataPath(_levelFile);
                        } else
                            if (_levelFile.getName().endsWith(".j3odata")) {
                                _level.getLoaderHandler().setMapInfoPath(_levelFile);
                            }
                    }
                    _level.initializeLevelInfo();
                    mapRef.add(_level);
                }
            }
        } else
            throw new NullPointerException("Levels directory not found !");
    }

    public void initializeLevelInfo() {
        Properties _props = new Properties();
        try {
            _props.load(new FileInputStream(loaderHandler.getMapInfoPath()));
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Unable to get level infos", ex);
        }

        mapId = Integer.valueOf(_props.getProperty("LEVEL_ID", "-1"));
        mapName = _props.getProperty("LEVEL_NAME", "NO_NAME");
    }

    public GameMapLoaderHandler getLoaderHandler() {
        return loaderHandler;
    }

    public int getMapId() {
        return mapId;
    }

    public String getMapName() {
        return mapName;
    }

    public static List<GameMap> getMapRef() {
        return mapRef;
    }

    public Spatial getLevel() {
        return level;
    }

    public void setLevel(Spatial level) {
        this.level = level;
    }

}
