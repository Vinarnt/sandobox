package com.omega.sandobox.client.game.level;

import com.jme3.asset.AssetEventListener;
import com.jme3.asset.AssetKey;
import com.jme3.asset.AssetNotFoundException;
import com.jme3.asset.DesktopAssetManager;
import com.jme3.asset.ModelKey;
import com.omega.sandobox.client.Constants;
import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.client.ui.screen.LoadingScreen;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import tonegod.gui.controls.text.Label;

/**
 *
 * @author Kyu
 */
public class GameMapLoaderHandler {

    private static final Logger logger = Logger.getLogger(GameMapLoaderHandler.class.getName());
    private static GameMap lastLoaded;
    private GameMap level;
    private File mapDataPath;
    private File mapInfoPath;
    private ModelKey levelKey;
    private LoadingScreen loadScreen;
    private Map<AssetKey, Label> loadingStates = new HashMap<>();

    public GameMapLoaderHandler(GameMap level) {
        this.level = level;
    }

    public boolean load() throws InterruptedException {
        loadScreen = Main.getGameCore().getStateManager().getState(LoadingState.class).getLoadingScreen();
        final DesktopAssetManager _assetMgr = (DesktopAssetManager) Main.getGameCore().getAssetManager();
        levelKey = new ModelKey(mapDataPath.getPath().replace('\\', '/').replace(Constants.ASSETS_FOLDER, ""));
        AssetEventListener _assetListener = new AssetEventListener() {
            @Override
            public void assetLoaded(final AssetKey key) {
                Main.getGameCore().enqueue(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        Label _label = loadingStates.get(key);
                        if (_label != null)
                            _label.setText("Load " + key + " : Ok");

                        return null;
                    }
                });
            }

            @Override
            public void assetRequested(final AssetKey key) {
                if (_assetMgr.getFromCache(key) != null)
                    return;

                Main.getGameCore().enqueue(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        Label _label = loadingStates.put(key, loadScreen.getNextLabel());
                        if (_label != null)
                            _label.setText("Load " + key + " : ...");

                        return null;
                    }
                });
            }

            @Override
            public void assetDependencyNotFound(AssetKey parentKey, AssetKey dependentAssetKey) {
                logger.log(Level.SEVERE, "Dependency not found : {0}", dependentAssetKey);
            }
        };
        _assetMgr.addAssetEventListener(_assetListener);
        Main.getGameCore().enqueue(new Callable<Void>() {
            @Override
            public Void call() {
                try {
                    level.setLevel(_assetMgr.loadModel(levelKey));
                } catch (AssetNotFoundException ex) {
                    logger.severe("Unable to load level : Asset not found");
                }
                synchronized (GameMapLoaderHandler.this) {
                    GameMapLoaderHandler.this.notify();
                }

                return null;
            }
        });
        synchronized (this) {
            wait();
        }
        lastLoaded = level;
        _assetMgr.removeAssetEventListener(_assetListener);

        return level != null;
    }

    public static GameMap getLastLoaded() {
        return lastLoaded;
    }

    public ModelKey getLevelKey() {
        return levelKey;
    }

    public void setMapDataPath(File mapDataPath) {
        this.mapDataPath = mapDataPath;
    }

    public void setMapInfoPath(File mapInfoPath) {
        this.mapInfoPath = mapInfoPath;
    }

    public File getMapDataPath() {
        return mapDataPath;
    }

    public File getMapInfoPath() {
        return mapInfoPath;
    }

}
