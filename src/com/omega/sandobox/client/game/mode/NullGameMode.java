package com.omega.sandobox.client.game.mode;

import com.omega.sandobox.client.game.mode.common.AbstractGameMode;
import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;

/**
 *
 * @author Kyu
 */
public class NullGameMode extends AbstractGameMode {

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public String getName() {
        return "All";
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public void cleanup() {
    }
    
}
