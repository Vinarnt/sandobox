package com.omega.sandobox.client.game.mode.common;

import com.omega.sandobox.client.game.mode.tdm.TeamDeathMatchGameMode;
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.omega.sandobox.client.game.mode.ctf.CaptureTheFlagGameMode;
import com.omega.sandobox.client.game.mode.ffa.FreeForAllGameMode;
import com.omega.sandobox.client.game.mode.NullGameMode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kyu
 */
public abstract class AbstractGameMode extends AbstractAppState {

    protected final static List<AbstractGameMode> gameModes;
    
    static {
        gameModes = new ArrayList<>();
        gameModes.add(new NullGameMode());
        gameModes.add(new TeamDeathMatchGameMode());
        gameModes.add(new CaptureTheFlagGameMode());
        gameModes.add(new FreeForAllGameMode());
    }
    
    @Override
    public abstract void initialize(AppStateManager stateManager, Application app);
    
    public abstract int getId();
    public abstract String getName();
    public abstract String getDescription();
    
    @Override
    public abstract void cleanup();
    
    public static List<AbstractGameMode> getGameModes() {
        return gameModes;
    }
    
    public static AbstractGameMode getGameModeById(int id) {
        for(AbstractGameMode _gameMode : gameModes) {
            if(_gameMode.getId() == id)
                return _gameMode;
        }
        return null;
    }
    
}
