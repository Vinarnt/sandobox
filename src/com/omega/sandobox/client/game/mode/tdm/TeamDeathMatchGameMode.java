package com.omega.sandobox.client.game.mode.tdm;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AppStateManager;
import com.omega.sandobox.client.appstate.LoadingState;
import com.omega.sandobox.client.game.mode.common.AbstractGameMode;
import com.omega.sandobox.client.input.CommonGameInputHandler;
import com.omega.sandobox.client.ui.screen.LoadingScreen;

/**
 *
 * @author Kyu
 */
public class TeamDeathMatchGameMode extends AbstractGameMode {

    private SimpleApplication app;
    private LoadingState loadState;
    private LoadingScreen loadScreen;
    private CommonGameInputHandler inputs;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        this.app = (SimpleApplication) app;
        loadState = app.getStateManager().getState(LoadingState.class);
        loadScreen = loadState.getLoadingScreen();
        
        inputs = new CommonGameInputHandler(this.app);
        inputs.initialize();
        
        initialized = true;
    }

    @Override
    public int getId() {
        return 1;
    }

    @Override
    public String getName() {
        return "Team Death Match";
    }

    @Override
    public String getDescription() {
        return "Description for tdm";
    }

    @Override
    public void cleanup() {
        inputs.cleanup();
        
        initialized = false;
    }
}
