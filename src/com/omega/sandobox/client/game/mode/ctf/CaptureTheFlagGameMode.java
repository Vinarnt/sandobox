package com.omega.sandobox.client.game.mode.ctf;

import com.omega.sandobox.client.game.mode.common.AbstractGameMode;
import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;

/**
 *
 * @author Kyu
 */
public class CaptureTheFlagGameMode extends AbstractGameMode {

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
    }

    @Override
    public int getId() {
        return 3;
    }

    @Override
    public String getName() {
        return "Capture The Flag";
    }

    @Override
    public String getDescription() {
        return "Description for ctf";
    }

    @Override
    public void cleanup() {
    }
}
