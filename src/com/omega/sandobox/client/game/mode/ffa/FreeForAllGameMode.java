package com.omega.sandobox.client.game.mode.ffa;

import com.omega.sandobox.client.game.mode.common.AbstractGameMode;
import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;

/**
 *
 * @author Kyu
 */
public class FreeForAllGameMode extends AbstractGameMode {

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
    }
    
    @Override
    public int getId() {
        return 2;
    }
    
    @Override
    public String getName() {
        return "Free For All";
    }
    
    @Override
    public String getDescription() {
        return "Description for ffa";
    }

    @Override
    public void cleanup() {
    }
}
