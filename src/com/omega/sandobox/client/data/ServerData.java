package com.omega.sandobox.client.data;

/**
 *
 * @author Kyu
 */
public class ServerData {

    private String name;
    private String host;
    private int port;
    private boolean isDedicated;
    private boolean hasPassword;
    private int currentPlayers;
    private int maxPlayers;
    private String gameMode;
    private String map;

    public ServerData(String name, String host, int port,
            boolean isDedicated, boolean hasPassword, int currentPlayers, int maxPlayers,
            String gameMode, String map) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.isDedicated = isDedicated;
        this.hasPassword = hasPassword;
        this.currentPlayers = currentPlayers;
        this.maxPlayers = maxPlayers;
        this.gameMode = gameMode;
        this.map = map;
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public boolean isDedicated() {
        return isDedicated;
    }

    public boolean hasPassword() {
        return hasPassword;
    }

    public int getCurrentPlayers() {
        return currentPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public String getGameMode() {
        return gameMode;
    }

    public String getMap() {
        return map;
    }
}
