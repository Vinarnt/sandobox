package com.omega.sandobox.client.data.xml;

import com.omega.sandobox.client.Constants;
import com.omega.sandobox.client.data.ServerFilterData;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Kyu
 */
public class ServerFilterFactory {

    private static final Logger logger = Logger.getLogger(ServerFilterFactory.class.getName());
    private ServerFilterData filters;
    private Document document;
    private Element root;
    private Node name;
    private Node hasPassword;
    private Node isDedicated;
    private Node isFull;
    private Node isEmpty;
    private Node gameMode;
    private Node mapNode;
    private Source source;
    private Result result;
    private TransformerFactory transformFactory;
    private Transformer transformer;
    private static ServerFilterFactory instance;

    public ServerFilterFactory() {
    }

    public static ServerFilterFactory getInstance() {
        if (instance == null)
            instance = new ServerFilterFactory();

        return instance;
    }

    public void initialize() {
        filters = new ServerFilterData();
        try {
            DocumentBuilderFactory _docBuildFact = DocumentBuilderFactory.newInstance();
            _docBuildFact.setValidating(true);
            _docBuildFact.setNamespaceAware(true);
            _docBuildFact.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                    "http://www.w3.org/2001/XMLSchema");
            DocumentBuilder _docBuilder = _docBuildFact.newDocumentBuilder();
            _docBuilder.setErrorHandler(new ServerFilterErrorHandler());
            document = _docBuilder.parse(Constants.SERVER_FILTER_PATH);
            root = document.getDocumentElement();
            source = new DOMSource(document);
            result = new StreamResult(Constants.SERVER_FILTER_PATH);
            transformFactory = TransformerFactory.newInstance();
            transformer = transformFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            logger.log(Level.WARNING, "Unable to load server filters", ex);
        } catch (TransformerConfigurationException ex) {
            logger.log(Level.WARNING, "Unable to get transformer", ex);
        }
    }

    public void load() {
        name = root.getElementsByTagName("name").item(0);
        hasPassword = root.getElementsByTagName("hasPassword").item(0);
        isDedicated = root.getElementsByTagName("isDedicated").item(0);
        isEmpty = root.getElementsByTagName("isEmpty").item(0);
        isFull = root.getElementsByTagName("isFull").item(0);
        gameMode = root.getElementsByTagName("gamemode").item(0);
        mapNode = root.getElementsByTagName("map").item(0);

        filters.setName(name.getTextContent());
        filters.setHasPassword(Boolean.valueOf(hasPassword.getTextContent()));
        filters.setIsDedicated(Boolean.valueOf(isDedicated.getTextContent()));
        filters.setIsEmpty(Boolean.valueOf(isEmpty.getTextContent()));
        filters.setIsFull(Boolean.valueOf(isFull.getTextContent()));
        filters.setGameMode(gameMode.getTextContent());
        filters.setMap(mapNode.getTextContent());
    }

    public void save() {
        name.setTextContent(filters.getName());
        hasPassword.setTextContent(String.valueOf(filters.hasPassword()));
        isDedicated.setTextContent(String.valueOf(filters.isDedicated()));
        isEmpty.setTextContent(String.valueOf(filters.isEmpty()));
        isFull.setTextContent(String.valueOf(filters.isFull()));
        gameMode.setTextContent(filters.getGameMode());
        mapNode.setTextContent(filters.getMap());
        try {
            transformer.transform(source, result);
        } catch (NullPointerException | TransformerException ex) {
            logger.log(Level.WARNING, "Can't save server filters", ex);
        }
    }

    public ServerFilterData getFilters() {
        return filters;
    }
}
