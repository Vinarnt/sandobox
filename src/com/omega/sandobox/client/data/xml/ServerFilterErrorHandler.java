package com.omega.sandobox.client.data.xml;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author Kyu
 */
public class ServerFilterErrorHandler implements ErrorHandler{

    private static final Logger logger = Logger.getLogger(ServerFilterErrorHandler.class.getName());
    
    @Override
    public void warning(SAXParseException exception) throws SAXException {
        logger.log(Level.WARNING, exception.getMessage());
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        logger.log(Level.WARNING, exception.getMessage());
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        logger.log(Level.SEVERE, "Severe error when parsing server filters xml", exception);
    }
    
}
