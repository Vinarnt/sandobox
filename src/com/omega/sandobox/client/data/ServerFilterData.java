package com.omega.sandobox.client.data;

/**
 *
 * @author Kyu
 */
public class ServerFilterData {

    private String name;
    private boolean hasPassword;
    private boolean isFull;
    private boolean isEmpty;
    private boolean isDedicated;
    private String gameMode;
    private String map;

    public ServerFilterData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasPassword() {
        return hasPassword;
    }

    public void setHasPassword(boolean hasPassword) {
        this.hasPassword = hasPassword;
    }

    public boolean isFull() {
        return isFull;
    }

    public void setIsFull(boolean isFull) {
        this.isFull = isFull;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    public boolean isDedicated() {
        return isDedicated;
    }

    public void setIsDedicated(boolean isDedicated) {
        this.isDedicated = isDedicated;
    }

    public String getGameMode() {
        return gameMode;
    }

    public void setGameMode(String gameMode) {
        this.gameMode = gameMode;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }
}
