package com.omega.sandobox.client.network.master.packet;

import com.omega.sandobox.client.data.ServerFilterData;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SERVERLIST 0x1 filters : - Name string - hasPassword boolean - isFull boolean - isEmpty
 * boolean - isDedicated boolean - gamemode short - map short
 *
 * @author Kyu
 */
public class ServerListPacket extends AbstractPacket {

    private static final Logger logger = Logger.getLogger(ServerListPacket.class.getName());
    public static final byte PACKET_ID = 0x1;
    // filters
    private final ServerFilterData filters;

    public ServerListPacket(ServerFilterData filters) {
        this.filters = filters;
    }

    @Override
    public void decode(ByteBufInputStream buffer, Channel channel) {
    }

    @Override
    public void execute(Channel channel) {
    }

    @Override
    public void encode(Channel channel) {
        try {
            out = new ByteBufOutputStream(Unpooled.buffer(1024));
            out.write(PACKET_ID);
            out.writeUTF(filters.getName());
            out.writeBoolean(filters.hasPassword());
            out.writeBoolean(filters.isFull());
            out.writeBoolean(filters.isEmpty());
            out.writeBoolean(filters.isDedicated());
            out.writeUTF((filters.getGameMode().equals("All")) ? "" : filters.getGameMode());
            out.writeUTF((filters.getMap().equals("All")) ? "" : filters.getMap());
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Unable to build packet", ex);
        }
    }
}
