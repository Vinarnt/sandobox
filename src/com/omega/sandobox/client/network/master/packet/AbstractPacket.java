package com.omega.sandobox.client.network.master.packet;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.Channel;

/**
 *
 * @author Kyu
 */
public abstract class AbstractPacket {

    protected ByteBufOutputStream out;

    public abstract void decode(ByteBufInputStream buffer, Channel channel);

    public abstract void execute(Channel channel);

    public abstract void encode(Channel channel);

    public ByteBufOutputStream getOut() {
        return out;
    }
}
