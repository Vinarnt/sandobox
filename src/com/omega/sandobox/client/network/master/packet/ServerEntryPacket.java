package com.omega.sandobox.client.network.master.packet;

import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.appstate.LobbyState;
import com.omega.sandobox.client.data.ServerData;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.Channel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class ServerEntryPacket extends AbstractPacket {

    private static final Logger logger = Logger.getLogger(ServerEntryPacket.class.getName());
    public static final byte PACKET_ID = 0x2;

    @Override
    public void decode(ByteBufInputStream buffer, Channel channel) {
        try {
            Main.getGameCore().getStateManager().getState(LobbyState.class).getScreen().addServerRow(
                    new ServerData(
                    buffer.readUTF(), 
                    buffer.readUTF(), 
                    buffer.readInt(), 
                    buffer.readBoolean(), 
                    buffer.readBoolean(), 
                    buffer.readInt(), 
                    buffer.readInt(), 
                    buffer.readUTF(), 
                    buffer.readUTF()));
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Cannot rebuild packet", ex);
        }
    }

    @Override
    public void execute(Channel channel) {
    }

    @Override
    public void encode(Channel channel) {
    }
}
