package com.omega.sandobox.client.network.master;

import com.omega.sandobox.client.network.master.packet.AbstractPacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class MasterClientHandler extends SimpleChannelInboundHandler<AbstractPacket> {

    private static final Logger logger = Logger.getLogger(MasterClientHandler.class.getName());

    public MasterClientHandler() {
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, AbstractPacket packet) throws Exception {
        Channel _channel = ctx.channel();
        logger.log(Level.INFO, "[Recv - {0}] << {1}", new Object[]{_channel.remoteAddress(), packet.getClass().getSimpleName()});
        packet.execute(_channel);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.severe(cause.getMessage());
        ctx.close();
    }
}
