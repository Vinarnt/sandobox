package com.omega.sandobox.client.network.master.codec;

import com.omega.sandobox.client.network.master.packet.ServerEntryPacket;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import java.util.List;


/**
 *
 * @author Kyu
 */
public class PacketDecoder extends MessageToMessageDecoder<ByteBuf> {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        try (ByteBufInputStream _buffer = new ByteBufInputStream(in)) {
            byte _packetID = _buffer.readByte();
            System.out.println("Recv packet id " + _packetID);
            switch(_packetID)
            {
                case ServerEntryPacket.PACKET_ID:
                    ServerEntryPacket _packet = new ServerEntryPacket();
                    _packet.decode(_buffer, ctx.channel());
                    out.add(_packet);
                    
                    break;
            }
        }
    }
    
}
