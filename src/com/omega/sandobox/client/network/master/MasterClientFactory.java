package com.omega.sandobox.client.network.master;

import com.omega.sandobox.client.Constants;
import com.omega.sandobox.client.data.ServerFilterData;
import com.omega.sandobox.client.data.xml.ServerFilterFactory;
import com.omega.sandobox.client.network.master.packet.AbstractPacket;
import com.omega.sandobox.client.network.master.packet.ServerListPacket;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class MasterClientFactory implements Runnable {

    private static final Logger logger = Logger.getLogger(MasterClientFactory.class.getName());
    private EventLoopGroup workerGroup;
    private ChannelFuture channelFuture;
    private Thread thread;
    private static MasterClientFactory instance;

    public MasterClientFactory() {
    }

    public static MasterClientFactory getInstance() {
        if (instance == null)
            instance = new MasterClientFactory();

        return instance;
    }

    @Override
    public void run() {
        try {
            workerGroup = new NioEventLoopGroup();
            channelFuture = new Bootstrap()
                    .group(workerGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new MasterClientInitializer())
                    .connect(Constants.MASTER_SERVER_HOST, Constants.MASTER_SERVER_PORT)
                    .sync();
            logger.info("Connection done");
            
            ServerFilterData _filters = ServerFilterFactory.getInstance().getFilters();
            ServerListPacket _packet = new ServerListPacket(_filters);
            send(_packet);

            channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException ex) {
            logger.log(Level.SEVERE, "Unable to connect to master server", ex);
        } finally {
            workerGroup.shutdownGracefully();
        }
    }

    public void connect() {
        if (channelFuture != null && channelFuture.channel().isOpen())
            return;

        thread = new Thread(this, "Master Server - Thread");
        thread.start();
    }

    public void close() {
        if (channelFuture != null && channelFuture.channel().isOpen()) {
            try {
                channelFuture.channel().disconnect().sync();
            } catch (InterruptedException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }

    public void send(AbstractPacket packet) {
        try {
            channelFuture.channel().writeAndFlush(packet);
        } catch (NullPointerException ex) {
            logger.log(Level.WARNING, "Unable to send packet", ex);
        }
    }
}