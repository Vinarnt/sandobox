package com.omega.sandobox.client.network.game.crypto;

import com.jme3.asset.AssetInfo;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Kyu
 */
public final class GameMapChecksumHandler {

    private static final String ALGORITHM = "SHA1";
    
    public GameMapChecksumHandler() {
    }

    public byte[] generateChecksum(AssetInfo levelInfo) throws NoSuchAlgorithmException, IOException {
        InputStream _is = levelInfo.openStream();

        MessageDigest _mDigest = MessageDigest.getInstance(ALGORITHM);
        byte[] _dataBytes = new byte[1024];

        int _nRead;
        while ((_nRead = _is.read(_dataBytes)) != -1) {
            _mDigest.update(_dataBytes, 0, _nRead);
        }
        byte[] _digestedBytes = _mDigest.digest();
        printHexa(_digestedBytes);
        
        return _digestedBytes;
    }
    
    private void printHexa(byte[] bytes) {
        StringBuilder _sBuild = new StringBuilder("");
        for (int i = 0; i < bytes.length; i++) {
            _sBuild.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        System.out.println("Digest : " + _sBuild.toString());
    }

}
