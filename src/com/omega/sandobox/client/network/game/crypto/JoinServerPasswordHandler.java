package com.omega.sandobox.client.network.game.crypto;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 *
 * @author Kyu
 */
public final class JoinServerPasswordHandler {

    private static final String ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final int KEY_LENGTH = 512;
    private static final int ITERATIONS = 20000;

    public byte[] encrypt(String toEncrypt, String useAsSalt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        if (toEncrypt.isEmpty()) {
            return null;
        }

        byte[] _enryptedPassword = getEncryptedPassword(toEncrypt, generateSalt(useAsSalt));

        return _enryptedPassword;
    }

    private byte[] getEncryptedPassword(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, ITERATIONS, KEY_LENGTH);
        SecretKeyFactory _skFactory = SecretKeyFactory.getInstance(ALGORITHM);
        byte[] _securePassword = _skFactory.generateSecret(spec).getEncoded();

        return _securePassword;
    }

    private byte[] generateSalt(String useAsSalt) {
        StringBuilder _sBuild = new StringBuilder();
        for (int i = 0; i < useAsSalt.length(); i += 2) {
            _sBuild.append(useAsSalt.charAt(i));
        }
        _sBuild.reverse();
        return _sBuild.toString().getBytes();
    }

}
