package com.omega.sandobox.client.network.game.crypto;

/**
 *
 * @author Kyu
 */
public final class CryptoManager {

    private JoinServerPasswordHandler jspHandler;
    private GameMapChecksumHandler gmChecksumHandler;
    private static CryptoManager instance;

    public CryptoManager() {
        jspHandler = new JoinServerPasswordHandler();
        gmChecksumHandler = new GameMapChecksumHandler();
    }

    public static CryptoManager getInstance() {
        if (instance == null)
            instance = new CryptoManager();

        return instance;
    }

    public JoinServerPasswordHandler getJoinServerPasswordHandlerHandler() {
        return jspHandler;
    }

    public GameMapChecksumHandler getGmChecksumHandler() {
        return gmChecksumHandler;
    }

}
