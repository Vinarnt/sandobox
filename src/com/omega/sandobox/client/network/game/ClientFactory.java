/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omega.sandobox.client.network.game;

import com.jme3.network.Client;
import com.jme3.network.ClientStateListener;
import com.jme3.network.ErrorListener;
import com.jme3.network.Network;
import com.omega.sandobox.client.Constants;
import com.omega.sandobox.client.data.ServerData;
import com.omega.sandobox.client.entity.EntityManager;
import com.omega.sandobox.network.game.message.ClientMessageListener;
import com.omega.sandobox.network.game.message.MessageRegister;
import com.omega.sandobox.network.game.message.NetworkMessage;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class ClientFactory {

    private static final Logger logger = Logger.getLogger(ClientFactory.class.getName());
    private Client client;
    private Callable<Void> onConnectedTask;
    private Callable<Void> onFailedTask;
    private ServerData targetServer;
    private static ClientFactory instance;

    public ClientFactory() {
        MessageRegister.register();
    }

    public static ClientFactory getInstance() {
        if (instance == null) {
            instance = new ClientFactory();
        }

        return instance;
    }

    public void connect(ServerData serverData) {
        targetServer = serverData;
        try {
            close();

            client = Network.connectToServer(
                    Constants.CLIENT_SERVER_NAME, 
                    Constants.CLIENT_SERVER_VERSION, 
                    serverData.getHost(), 
                    serverData.getPort()
            );
            client.addMessageListener(new ClientMessageListener());
            client.addClientStateListener(new ClientStateListener() {
                @Override
                public void clientConnected(Client client) {
                    logger.info("Client connected");
                    try {
                        onConnectedTask.call();
                    } catch (Exception ex) {
                        logger.log(Level.SEVERE, null, ex);
                    }
                }

                @Override
                public void clientDisconnected(Client client, ClientStateListener.DisconnectInfo info) {
                    logger.log(Level.INFO, "Client disconnected (Reason : {0})", info);
                }
            });
            client.addErrorListener(new ErrorListener<Client>() {
                @Override
                public void handleError(Client source, Throwable t) {
                    try {
                        logger.log(Level.WARNING, "Network error : {0}", t.getMessage());
                        source.close();
                        onFailedTask.call();
                    } catch (Exception ex) {
                        Logger.getLogger(ClientFactory.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            client.start();
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Unable to connect to host {0}:{1}", new Object[]{serverData.getHost(), serverData.getPort()});
            try {
                onFailedTask.call();
            } catch (Exception ex1) {
                logger.log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void close() {
        if (client != null && client.isConnected())
            client.close();
        EntityManager _entityManager = EntityManager.getInstance();
        if (_entityManager != null)
            _entityManager.close();
    }

    public void setOnConnectedTask(Callable<Void> task) {
        this.onConnectedTask = task;
    }

    public void setOnFailedTask(Callable<Void> onFailedTask) {
        this.onFailedTask = onFailedTask;
    }

    public ServerData getTargetServer() {
        return targetServer;
    }

    public void send(NetworkMessage message) {
        if (client != null && client.isConnected()) {
            logger.log(Level.INFO, "SEND >> {0}", message);
            client.send(message);
        }
    }

    public Client getClient() {
        return client;
    }

}
