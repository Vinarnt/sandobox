/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omega.sandobox.client.appstate;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.light.DirectionalLight;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.omega.sandobox.client.controls.ThirdPersonCameraControl;
import com.omega.sandobox.client.input.CommonGameInputHandler;
import tonegod.gui.core.Screen;

/**
 *
 * @author Kyu
 */
public class GameState extends AbstractAppState {

    private CommonGameInputHandler inputHandler;
    private SimpleApplication app;
    private BulletAppState physicsSpace;
    private DirectionalLight sunLight;
    
    private ThirdPersonCameraControl cameraControl;
    
    private Screen gameScreen;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        this.app = (SimpleApplication) app;

        this.app.getFlyByCamera().setEnabled(false);
        
        inputHandler = new CommonGameInputHandler(this.app);
        inputHandler.initialize();
        
        // Init GUI
        gameScreen = new Screen(app);
        this.app.getGuiNode().addControl(gameScreen);
        
        Node _scene = (Node) app.getAssetManager().loadModel("levels/level1/scene1.j3o");
        this.app.getRootNode().attachChild(_scene);

        physicsSpace = new BulletAppState();
        stateManager.attach(physicsSpace);
        physicsSpace.setDebugEnabled(true);

        _scene.addControl(new RigidBodyControl(0f));
        physicsSpace.getPhysicsSpace().add(_scene);

        Spatial _player = this.app.getAssetManager().loadModel("Models/Sinbad/Sinbad.mesh.j3o");
        _player.setName("sinbad");
        _player.setLocalTranslation(0f, 10f, 0f);
        _player.scale(0.3f);
        _player.addControl(new BetterCharacterControl(0.5f, 2f, 9.81f));
        cameraControl = new ThirdPersonCameraControl(this.app.getCamera(), this.app.getInputManager());
        _player.addControl(cameraControl);
        physicsSpace.getPhysicsSpace().add(_player);
        this.app.getRootNode().attachChild(_player);
        //this.app.getFlyByCamera().setMoveSpeed(100f);

        sunLight = new DirectionalLight();
        this.app.getRootNode().addLight(sunLight);

        super.initialize(stateManager, app);
    }

    @Override
    public void update(float tpf) {
        //TODO: implement behavior during runtime
    }

    @Override
    public void cleanup() {
        inputHandler.cleanup();

        super.cleanup();
    }

    public CommonGameInputHandler getInputHandler() {
        return inputHandler;
    }
    
    public ThirdPersonCameraControl getCameraControl()
    {
        return cameraControl;
    }
    
}
