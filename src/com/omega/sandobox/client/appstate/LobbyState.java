package com.omega.sandobox.client.appstate;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.omega.sandobox.client.ui.screen.lobby.LobbyScreen;
import java.util.logging.Logger;
import tonegod.gui.core.Screen;

/**
 *
 * @author Kyu
 */
public class LobbyState extends AbstractAppState {

    private static final Logger logger = Logger.getLogger(LobbyState.class.getName());
    private SimpleApplication app;
    private Screen rootScreen;
    private LobbyScreen screen;

    public LobbyState(Screen rootScreen) {
        this.rootScreen = rootScreen;
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        this.app = (SimpleApplication) app;

        screen = new LobbyScreen(this.app, rootScreen);

        rootScreen.addElement(screen);
        
        setEnabled(false);
        super.initialize(stateManager, app);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (enabled) {
            show();
        } else {
            hide();
        }
    }

    public void show() {
        screen.show();
    }

    public void hide() {
        screen.hide();
    }

    @Override
    public void update(float tpf) {
    }

    @Override
    public void cleanup() {
        hide();

        super.cleanup();
    }

    public LobbyScreen getScreen() {
        return screen;
    }
}
