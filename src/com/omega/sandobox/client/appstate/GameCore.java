/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omega.sandobox.client.appstate;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.ClasspathLocator;
import com.omega.sandobox.client.Constants;
import com.omega.sandobox.client.game.level.GameMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.log4j.BasicConfigurator;
import strongdk.jme.appstate.console.CommandEvent;
import strongdk.jme.appstate.console.CommandListener;
import strongdk.jme.appstate.console.CommandParser;
import strongdk.jme.appstate.console.ConsoleAppState;
import strongdk.jme.appstate.console.ConsoleDefaultCommandsAppState;
import tonegod.gui.core.Screen;

/**
 *
 * @author Kyu
 */
public class GameCore extends SimpleApplication {

    private ConsoleAppState console;
    private Screen screen;

    @Override
    public void simpleInitApp() {
        Logger.getLogger("").setLevel(Level.ALL);
        BasicConfigurator.configure();
        
        // Delete default input mappings
        inputManager.clearMappings();
        
        // Add locator to AssetManager
        assetManager.registerLocator(Constants.ASSETS_FOLDER, ClasspathLocator.class);
        
        // Load levels reference
        GameMap.referenceMaps();

        console = new ConsoleAppState() {
            @Override
            public void setVisible(boolean setVisible) {
                super.setVisible(setVisible);

                stateManager.getState(GameState.class).getInputHandler().setInterfaceMode(setVisible);
            }
        };
        CommandListener _cmdListener = new CommandListener() {
            @Override
            public void execute(CommandEvent event) {
                final CommandParser parser = event.getParser();
                if (event.getCommand().equals("speed")) {
                    Integer _value = parser.getInt(0);
                    if (_value != null) {
                        GameCore.this.flyCam.setMoveSpeed(_value);
                        console.appendConsole("Camera speed changed: " + _value);
                    } else {
                        console.appendConsoleError("Could not change speed, not a valid number: " + parser.getString(0));
                    }
                }
                else if (event.getCommand().equals("fullscreen")) {
                    String _value = parser.getString(0);
                    if (_value != null && (_value.equalsIgnoreCase("true") || _value.equalsIgnoreCase("false"))) {
                        boolean _bool = Boolean.parseBoolean(_value);
                        settings.setFullscreen(_bool);
                        restart();
                        if(_bool)
                            console.appendConsole("Fullscreen mode applied.");
                        else
                            console.appendConsole("Windowed mode applied.");
                    } else {
                        console.appendConsoleError("Could not set to fullscreen, arguments must be true/false.");
                    }
                }

            }
        };
        console.registerCommand("speed", _cmdListener);
        console.registerCommand("fullscreen", _cmdListener);

        stateManager.attach(console);

        stateManager.attach(
                new ConsoleDefaultCommandsAppState());

        screen = new Screen(this);
        getGuiNode().addControl(screen);
        
        stateManager.attach(new MainMenuState(screen));
        stateManager.attach(new LobbyState(screen));
        screen.setGlobalUIScale(
                screen.getWidth() / Constants.UI_BASE_WIDTH, 
                screen.getHeight() / Constants.UI_BASE_HEIGHT
        );
    }


    @Override
    public void destroy() {
        super.destroy();
        
        System.exit(0);
    }
    
}
