package com.omega.sandobox.client.appstate;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.omega.sandobox.client.ui.screen.MainMenuScreen;
import tonegod.gui.core.Screen;

/**
 *
 * @author Kyu
 */
public class MainMenuState extends AbstractAppState {

    private SimpleApplication app;
    private Screen rootScreen;
    private MainMenuScreen screen;

    public MainMenuState(Screen rootScreen) {
        this.rootScreen = rootScreen;
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        this.app = (SimpleApplication) app;
        this.screen = new MainMenuScreen(this.app, rootScreen);
        
        rootScreen.addElement(screen);

        super.initialize(stateManager, app);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (enabled)
            show();
        else
            hide();
    }

    private void show() {
        screen.show();
    }

    private void hide() {
        screen.hide();
    }

    @Override
    public void cleanup() {
        super.cleanup();

        hide();
    }
}
