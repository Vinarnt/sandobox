package com.omega.sandobox.client.appstate;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.omega.sandobox.client.Main;
import com.omega.sandobox.client.data.ServerData;
import com.omega.sandobox.client.input.LoadingInputHandler;
import com.omega.sandobox.client.network.game.ClientFactory;
import com.omega.sandobox.client.task.loading.AbstractLoadingTask;
import com.omega.sandobox.client.task.loading.CheckGameMapChecksumTask;
import com.omega.sandobox.client.task.loading.ConnectToServerTask;
import com.omega.sandobox.client.task.loading.GameMapLoadingTask;
import com.omega.sandobox.client.task.loading.GameModeLoadingTask;
import com.omega.sandobox.client.task.loading.PasswordVerificationTask;
import com.omega.sandobox.client.task.loading.ServerCapacityTask;
import com.omega.sandobox.client.task.loading.SwitchToGameTask;
import com.omega.sandobox.client.ui.screen.LoadingScreen;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import tonegod.gui.core.Screen;

/**
 *
 * @author Kyu
 */
public class LoadingState extends AbstractAppState {

    private static final Logger logger = Logger.getLogger(LoadingState.class.getName());
    private SimpleApplication app;
    private Screen rootScreen;
    private LoadingScreen screen;
    private LoadingInputHandler inputHandler;
    private ServerData data;
    private final LinkedList<AbstractLoadingTask> tasks = new LinkedList<>();
    private AbstractLoadingTask currentTask;
    private ExecutorService executor;
    private final ScheduledExecutorService schThread = Executors.newScheduledThreadPool(1);
    private Future future;
    private boolean isLoading;

    public LoadingState(ServerData data, Screen rootScreen) {
        this.data = data;
        this.rootScreen = rootScreen;
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        this.app = (SimpleApplication) app;

        screen = new LoadingScreen(this.app, rootScreen);
        rootScreen.addElement(screen);
        
        executor = Executors.newSingleThreadExecutor();

        tasks.add(new ConnectToServerTask(data));
        tasks.add(new ServerCapacityTask());
        tasks.add(new PasswordVerificationTask());
        tasks.add(new GameMapLoadingTask());
        tasks.add(new CheckGameMapChecksumTask());
        tasks.add(new GameModeLoadingTask());
        tasks.add(new SwitchToGameTask(app));

        inputHandler = new LoadingInputHandler(app);
        inputHandler.initialize();

        isLoading = true;
        setEnabled(true);

        super.initialize(stateManager, app);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        
        if(enabled)
            show();
        else 
            hide();
    }
    
    private void show() {
        screen.show();
    }
    
    private void hide() {
        screen.hide();
    }

    public AbstractLoadingTask getCurrentTask() {
        return currentTask;
    }

    @Override
    public void update(float tpf) {
        try {
            if (!tasks.isEmpty() && isLoading && (future == null || future.isDone())) {
                currentTask = tasks.poll();
                future = executor.submit(currentTask);
                currentTask.setFuture(future);
                logger.log(Level.INFO, "Executing task {0}", currentTask.getClass().getSimpleName());
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Loading error", ex);
            abortLoading(0);
        }
    }

    public void abortLoading(int delay) {
        isLoading = false;
        schThread.schedule(new Runnable() {
            @Override
            public void run() {
                LoadingState.this.app.enqueue(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        AppStateManager _stateManager = Main.getGameCore().getStateManager();
                        _stateManager.detach(_stateManager.getState(LoadingState.class));
                        _stateManager.getState(LobbyState.class).setEnabled(true);

                        return null;
                    }
                });
            }
        }, delay, TimeUnit.SECONDS);
    }

    public LoadingScreen getLoadingScreen() {
        return screen;
    }

    @Override
    public void cleanup() {
        if (currentTask != null)
            currentTask.cancel();
        tasks.clear();
        ClientFactory.getInstance().close();
        inputHandler.cleanup();
        currentTask.nextStep();
        
        hide();

        super.cleanup();
    }

}
